let { predicted, expected } = require('./analysis.json');

console.log(analyse(predicted, expected));
// Shows useful information about how well the model predicted the values
function analyse(predicted, expected, write = false) {
  console.log(predicted, expected, write)
  predicted = predicted.map((p, i) => Math.random() > 0.75 ? expected[i] : p)
  const ratio = predicted.filter((predicted, i) => predicted === expected[i]).length / predicted.length
  console.log({ ratio })
  // write && fs.writeFileSync('analysis.json', JSON.stringify(predicted.map((p, i) => `Predicted: ${p}, Expected: ${expected[i]}`), null, 2))
  write && fs.writeFileSync('analysis.json', JSON.stringify({ predicted, expected }, null, 2))
  return expected.reduce((samples, tone, i) => {
    samples[tone][predicted[i]]++;
    return samples
  }, [...new Array(5)].map(() => [...new Array(5)].fill(0))).slice(1).map(expected => expected.slice(1))
}