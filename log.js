const { Matrix } = require('ml-matrix');
const fs = require('fs');

/*
log,
  features
  labels, integer, 0-256
fnn,
  features array, integer, 0-256
  labels: array, integer, 0-256

*/

module.exports = class Model {

  constructor(type = "svm", data, labels, options) {
    this.type = type;
    this.options = options;
    let Model
    if (type === "log") {
      Model = require('ml-logistic-regression');
    } else if (type === "fnn") {
      Model = require('ml-fnn')
    } else if (type === "svm") {
      Model = require('libsvm-js/asm')
    } else if (type === "knn") {
      Model = require('ml-knn')
    } else if (type === "gnb") {
      Model = require('ml-naivebayes').GaussianNB
    } else if (type === "mnb") {
      Model = require('ml-naivebayes').MultinomialNB
    } else {
      console.error("Type must be one of ['svm', 'fnn', 'log']")
    }
    if (type === "knn") {
      this.model = new Model(data, labels, options)
    } else {
      this.model = new Model(options)
      this.train(data, labels)
    }
  }

  train(features, labels) {
    features = this.normalize(features);
    // if (this.type === 'fnn') {
    //   labels = labels.map(label => {
    //     let array = [0, 0, 0, 0, 0]
    //     array[label] = 1;
    //     return array;
    //   })
    // } {
    //   labels = Matrix.columnVector(labels);
    // }

    this.model.train(features, labels)
    return this;
  }

  predict(features) {
    features = this.normalize(features);
      // features = new Matrix(features);
    const predicted = this.model.predict(features);
    return this.type === "fnn" ? predicted.map(label => {
      return label[0].indexOf(1)
    }) : predicted
  }

  normalize(data) {
    let matrix = new Matrix(data);
    // const d = data.reduce((d, features, i) => features.map((feature, i) => d[i].concat(feature)))
    // const mins = d.map(features => Math.min(...features))
    // const maxs = d.map(features => Math.max(...features))

    // data = data.map(features)
    // Xnew = (X- Xmin)/(Xmax-Xmin)
    let bounds = [0, 1]
    if (this.type === 'log') {
      bounds = [0, 4]
    } else if (this.type === 'fnn') {
      bounds = [0, 255]
    }
    matrix = matrix.scaleColumns(...bounds);
    return this.type === "fnn" ? matrix.round() : matrix

  }
  standardize(features) {
    // Xnew=x-mean/std
    return features;
  }
}