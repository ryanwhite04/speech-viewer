const { Matrix } = require('ml-matrix')
const fs = require('fs')
const FNN = require("ml-fnn")
const data = require("./data.json")
  // .slice(0, 1000)

const model = new FNN({
  hiddenLayers: [10, 10],
  iterations: 50,
  learningRate: 0.01, // Also known as epsilon
  reglarization: 0.001,
  activation: 'logistic', // 'tanh'(default), 'identity', 'logistic', 'arctan', 'softsign', 'relu', 'softplus', 'bent', 'sinusoid', 'sinc', 'gaussian'). (single-parametric options: 'parametric-relu', 'exponential-relu', 'soft-exponential').
  activationParam: 1,
})

const ratio = 4;
const trainingData = data.filter((syllable, i) => i % ratio)
const testingData = data.filter((syllable, i) => !(i % ratio))

train(trainingData);
test(testingData)

function train(data) {
  // let features = new Matrix(data.map(({ features }) => features));
  let features = new Matrix(data.map(({ label }) => {
    console.log({ label })
    let labels = [0, 0, 0, 0, 0];
    labels[label] = 1;
    return labels
  }));
  // features = features.scaleColumns(0, 10).round()
  // let labels = Matrix.columnVector(data.map(({ label }) => label === 3 ? 1 : 0))
  let labels = Matrix.columnVector(data.map(({ label }) => label))
  model.train(features, labels)
}

function test(data) {
  // let features = new Matrix(data.map(({ features }) => features));
  // let features = new Matrix(data.map(({ label }) => label));

  // features = features.scaleColumns(0, 10).round()
  // let labels = Matrix.columnVector(data.map(({ label }) => label === 3 ? 1 : 0))
  let features = new Matrix(data.map(({ label }) => {
    console.log({ label })
    let labels = [0, 0, 0, 0, 0];
    labels[label] = 1;
    return labels
  }));
  
  const predicted = model.predict(features);
  // console.log(predicted.filter(([value]) => value).length, labels.filter(([value]) => value).length)
  let expected = Matrix.columnVector(data.map(({ label }) => label))
  const ratio = predicted.filter((predicted, i) => predicted === expected[i]).length / predicted.length
  console.log(ratio, predicted.slice(10))
}

