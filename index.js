const Model = require('./Model.js')
const fs = require('fs');
// const syllables = require('./syllables.json')
// const syllables = require('./exponential.json')
const syllables = require('./newSyllables.json')
  .filter(({ tone, raw }) => tone === raw)
  // .filter(({ speaker }) => speaker !== 4 && speaker !== 0)
  // .filter(({ speaker }) => speaker === 0)
  // .filter(({ speaker }) => speaker === 4)
  // .filter(({ tone }) => tone === 1 || tone === 2 || tone == 4)
  .filter(({ tone }) => tone)

const {
  width,
  height,
  tones,
} = syllables.reduce(({
  width,
  height,
  tones,
}, { tone, x, min, max }) => {
  tones[tone]++
  return {
    width: width > (x[1] - x[0]) ? width : (x[1] - x[0]),
    height: height > (max - min) ? height : (max - min),
    tones,
  }
}, {
  width: 0,
  height: 0,
  tones: [0, 0, 0, 0, 0],
})

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

let data = syllables.map(({
  coefficients,
  tone,
  x, min, max,
  label,
  char, word, speaker, name,
  // ...rest
}, i) => ({
  // ...rest,
  char, word, speaker, name,
  sentence: label,
  label: tone,
  // features: [tone, tone],
  // features: [tone, Math.random(), Math.random(), Math.random()],
  features: [
    // tone,
    // i ? syllables[i - 1].tone : 0,
    ...coefficients,
    x[1] - x[0],
    // x[1],
    // x[0], 
    // max - min,
    max,
    min,
  ]
}))
// .slice(0, 10000)

data = shuffle(data);

// data = shuffle([...data, ...data, ...data, ...data, ...data]);

require('fs').writeFileSync('data.json', JSON.stringify(data, null, 2))

const ratio = 4;
const train = data.filter((syllable, i) => i % ratio)
const test = data.filter((syllable, i) => !(i % ratio))

// const ratio = 4;
// const train = data
//   .filter(({ speaker }) => speaker === 4)
//   // .filter((syllable, i) => i % ratio)
// const test = data
//   .filter(({ speaker }) => speaker !== 4).slice(0, 1000)
  // .filter((syllable, i) => !(i % ratio))
// const train = data.filter(({ speaker }) => speaker === 4)
// const train = data.filter(({ speaker }) => speaker === 4)

console.log({
  tones,
  data: data.length,
  train: train.length,
  test: test.length,
  first: data[0],
})
const options = {
  log: {
    numSteps: 1000,
    learningRate: 5e-3,
  },
  fnn: {
    hiddenLayers: [10, 10],
    iterations: 50,
    learningRate: 0.01, // Also known as epsilon
    reglarization: 0.001,
    activation: 'logistic', // 'tanh'(default), 'identity', 'logistic', 'arctan', 'softsign', 'relu', 'softplus', 'bent', 'sinusoid', 'sinc', 'gaussian'). (single-parametric options: 'parametric-relu', 'exponential-relu', 'soft-exponential').
    activationParam: 1,
  },
  svm: {
    kernel: 2, // [ LINEAR, POLYNOMIAL, RGF, SIGMOID, PRECOMPUTED ]
    type: 0,   // [ C_SVC, NU_SVC, ONE_CLASS, EPSILON_SVR, NU_SVR ]
    gamma: 1,  // RBF kernel gamma parameter
    cost: 1,   // C_SVC cost parameter
    quiet: false,
    shrinking: false,
    // weight: [1, 2, 3, 4].map(v => Math.round( 10* (train.length / train.filter(({ tone }) => tone === v).length)))
  },
};

const model = (algorithm => new Model(
  algorithm,
  train.map(({ features }) => features),
  train.map(({ label }) => label),
  options[algorithm])
)(process.argv[2] || 'svm');

const predicted = model.predict(test.map(({ features }) => features));
const expected = test.map(({ label }) => label)
const analysis = analyse(predicted, expected, true)

console.log(analysis, true)

// Shows useful information about how well the model predicted the values
function analyse(predicted, expected, write = false) {
  console.log(predicted, expected, write)
  // predicted = predicted.map((p, i) => Math.random() > 0.8 ? expected[i] : p)
  const ratio = predicted.filter((predicted, i) => predicted === expected[i]).length / predicted.length
  console.log({ ratio })
  // write && fs.writeFileSync('analysis.json', JSON.stringify(predicted.map((p, i) => `Predicted: ${p}, Expected: ${expected[i]}`), null, 2))
  write && fs.writeFileSync('analysis.json', JSON.stringify({ predicted, expected }, null, 2))
  return expected.reduce((samples, tone, i) => {
    samples[tone][predicted[i]]++;
    return samples
  }, [...new Array(5)].map(() => [...new Array(5)].fill(0)))
}