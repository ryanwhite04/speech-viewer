import React, { Component } from 'react'
import './App.css'
import debug from 'debug'
import generate from './utils/generate2'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {
  LineChart,
  Line,
  Label,
  CartesianGrid,
  Tooltip,
  ReferenceLine,
  ReferenceArea,
  XAxis,
  YAxis,
 } from 'recharts'
 import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'
import { List, ListItem } from 'material-ui/List'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import SvgIcon from 'material-ui/SvgIcon'
import TextField from 'material-ui/TextField'
import foo from 'speech-samples/speakers/1/index.json'

const speakers = [foo];

export default class App extends Component {

  state = {
    width: 1000,
    height: 500,
    domain: [0, 'auto'],
    sentence: 0,
    speaker: 0,
    key: 'time',
    tone: 0,
    regression: {
      order: 3,
      type: 'robust',
    }
  }

  render() {

    const log = debug('app');
    const {
      width, height,
      domain, key,
      tone, speaker,
      sentence, regression
    } = this.state
    
    log({ speaker, speakers })
   
    let { sentences, frames, labels, ...rest } = speakers[speaker];

    // console.log(generate);
    let tags = sentences[sentence];
    frames = frames[sentence].map(([intensity, nCandidates, strength, frequency]) => frequency);
    labels = labels[sentence];
    const { syllables, contour, model } = generate(regression)({ tags, frames });
    
    const data = [
      { color: "rgb(127, 127, 127)" },
      { color: "rgb(0, 127, 0)" },
      { color: "rgb(0, 0, 127)" },
      { color: "rgb(127, 0, 0)" },
      { color: "rgb(0, 0, 0)" }
    ].map(unpack(syllables))

    const filtered = syllables.filter(tone ? ({ tone: t }) => tone === t : () => true)
    log('render', this.state, rest, syllables, filtered, model)

    const onChange = ({ target: { name } }, v) => {
      console.log('onChange', { [name]: v })
      this.setState({ [name]: parseInt(v, 10) })
    }
    const actions = [
      {
        key: 'sentence',
        floatingLabelText: "Sentence",
        type: "number",
        min: "0",
        max: "1000",
        value: sentence,
        onChange,
      },
      {
        key: 'tone',
        floatingLabelText: "Tone",
        type: "number",
        min: "0",
        max: "5",
        value: tone,
        onChange,
      },
      {
        key: 'width',
        floatingLabelText: "Width",
        type: "number",
        min: "360",
        max: "3000",
        value: width,
        onChange,
      },
      {
        key: 'start',
        floatingLabelText: "Start",
        type: "number",
        min: "0",
        max: domain[1],
        value: domain[0],
        onChange,
      },
      {
        key: 'end',
        floatingLabelText: "End",
        type: "number",
        min: domain[0],
        max: contour.length,
        value: typeof domain[1] === 'number' ? domain[1] : contour.length,
        onChange,
      }
    ]

    return <MuiThemeProvider><div className="App">
      <h2>Sentence {sentence}</h2>
      <LineChart className="LineChart" width={width} height={height} data={contour}>
        <CartesianGrid />
        <Tooltip cursor={{strokeDasharray: '3 3'}} />
        <Line dot={false} type="monotone" dataKey="frequency" stroke="#8884d8" animationDuration={300} />
        <XAxis allowDataOverflow={true} domain={domain} dataKey="name" type="number" name="Time" unit="0ms">
          <Label value="Time" offset={-15} position="insideBottom" />
        </XAxis>
        <YAxis
          allowDataOverflow={true}
          dataKey="frequency"
          type="number"
          name="Frequency"
          unit="Hz"
          label={{
            value: 'Fundamental Frequency',
            angle: -90,
            position: 'insideLeft',
            offset: -15
          }}
        />
          {filtered.map(line)}
          {filtered.map(area)}
      </LineChart>
      {actions.map(({ key, ...action }) => <TextField key={key} name={key} className="Input" {...action} />)}
      <canvas id="canvas"></canvas>
    </div></MuiThemeProvider>
  }
}

function round(number, precision) {
  var factor = Math.pow(10, precision);
  var tempNumber = number * factor;
  var roundedTempNumber = Math.round(tempNumber);
  return roundedTempNumber / factor;
}

function display(data, label = 'Data') {
  const other = ['string', 'number'].includes(typeof data) ?
    { secondaryText: data } : typeof data === 'object' ?
    { nestedItems: Object.entries(data).map(([key, value]) => display(value, key)) } : {}
  return <ListItem className="ListItem" key={label} primaryText={`${label}`} {...other}/>
}

function sortBy(key, pivot = '.') {
  key = key.toLowerCase()
  return (a, b) => key.includes(pivot) ?
    a[key.split(pivot)[0]][key.split(pivot)[1]] > b[key.split(pivot)[0]][key.split(pivot)[1]] ? 1 : -1 :
    a[key] > b[key] ? 1 : -1
}

function row({ time, tone, name, coefficients }, header) {
  
  function column(header = false) {
    return (datum, key) => {
      return header ?
        <TableHeaderColumn key={key}>{datum}</TableHeaderColumn> :
        <TableRowColumn key={key}>{round(datum, 3)}</TableRowColumn>
    }
  }
  const columns = [time, tone, name, ...coefficients].map(column(header))
  
  return <TableRow key={name}>{columns}</TableRow>
}

function line({ time, name, tone }) {
  // These show when the clicks took place
  // As you can see, she didn't tag them "mid" syllable as she was suppose to
  // If a sentence has all these lines mid syllable, the "getRange" function would need to be updated
  console.log('line', { name, tone })
  return <ReferenceLine {...{
    x: time + 15,
    key: 'line_' + name,
    stroke: [
      null,
      'yellow',
      'blue',
      'red',
      'black',
      'green',
    ][tone] || "#82ca9d",
  }}><Label position="top">{name}</Label></ReferenceLine>
}

function area({ tone, name, x, min, max }) {
  // These are the boxes that border the tone contour of each syllable
  console.log('area', { name, tone })
  return <ReferenceArea {...{
    x1: x[0],
    x2: x[1] - 1,
    y1: min,
    y2: max,
    key: 'area_' + name,
    stroke: [
      null,
      'yellow',
      'blue',
      'red',
      'black',
      'green',
    ][tone],
    strokeOpacity: 0.3,
  }}><Label position="bottom">{tone}</Label></ReferenceArea>
}

function unpack(syllables) {
  return (marker = {}, i = 0) => {
    const filtered = syllables.filter(({ tone }) => parseInt(tone, 10) === i);

    console.log(i, filtered);

    const reduced = filtered.reduce(
      ({ x, y, z }, { coefficients: [a, b, c] }) => ({
        x: [...x, a],
        y: [...y, b],
        z: [...z, c]
      }),
      { x: [], y: [], z: [] }
    );

    console.log(i, reduced);

    return {
      type: "scatter3d",
      mode: "markers",
      marker: {
        size: 12,
        line: {
          color: "rgba(217, 217, 217, 0.14)",
          width: 0.5
        },
        opacity: 0.8,
        ...marker
      },
      ...reduced
    };
  };
}
