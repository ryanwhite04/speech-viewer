
function main(data) {
  // Notice there is no 'import' statement. 'tf' is available on the index-page
  // because of the script tag above.

  // Define a model for linear regression.
  const model = tf.sequential();
  model.add(tf.layers.dense({
    units: 5,
    // inputShape: [4]
    inputShape: [1]
  }));
  model.add(tf.layers.batchNormalization())
  model.add(tf.layers.softmax({
    units: 5
  }))

  // Prepare the model for training: Specify the loss and the optimizer.
  model.compile({
    loss: 'meanSquaredError',
    optimizer: 'sgd'
  });

  data = data.slice(0, 1000)
  const ratio = 4;
  const train = data.filter((syllable, i) => i % ratio)
  const test = data.filter((syllable, i) => !(i % ratio))

  // Generate some synthetic data for training.
  // const xs = tf.tensor2d(train.map(({ features }) => features));
  // const xs = tf.tensor2d(train.map(({ label }) => [label, label/2, label ^2, label +3]));
  const xs = tf.tensor2d(train.map(({ label }) => [label]));


  const ys = tf.tensor2d(train.map(({ label }) => {
    let labels = [0, 0, 0, 0, 0]
    labels[label] = 1;
    return labels;
  }));

  // Train the model using the data.
  model.fit(xs, ys, {
    epochs: 100
  }).then(() => {
    // Use the model to do inference on a data point the model hasn't seen before:
    // Open the browser devtools to see the output
    // const features = tf.tensor2d(test.map(({ features }) => features))
    // const features = tf.tensor2d(test.map(({ label }) => [label, label/2, label ^2, label +3]))
    const features = tf.tensor2d(test.map(({ label }) => [label]))
    const prediction = model.predict(features);
    prediction.print();
    const predicted = tf.unstack(prediction).map(e => tf.argMax(e)).map(d => d.dataSync()[0])
    const expected = test.map(({ label }) => label)
    const ratio = predicted.filter((predicted, i) => predicted === expected[i]).length / predicted.length
    console.log({ prediction, model, predicted, expected, ratio })
  });
}

fetch('data.json').then(body => body.json()).then(main)