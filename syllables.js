const generate = require('./src/utils/generate')({
  order: 2,
  type: 'robust',
});

// returns an array of normalized values describing a syllable, feature array
function extract(syllable) {
  // console.log('extract')
  
  // This is the logistic function, "A sigmoid function".
  // Scaled the input to a result between 0 and 1
  function squash(x) {
    return 1 / (1 + Math.E**(-x))
  }

  const { coefficients, max, min } = syllable
  return [...coefficients, max - min].map(squash)
}

// returns syllable data generated from sentence frames and tags
function flatten(sentences, generate) {
  return sentences.reduce((syllables, { speaker, label, ...sentence }) => {
    return syllables.concat(generate(sentence, {
      speaker, label
    }).syllables.map(syllable => ({ ...syllable, label, speaker })))
  }, [])
}

module.exports = flatten(require(`speech-samples/index.json`)
    // Combine the sentences, frames, and labels of each speaker into one array
    .map(({ sentences, frames, labels, ...rest }) => {

      // Each sample has its tags, frames and a label
      return sentences.map((sentence, i) => ({
        tags: sentence.map(({ time, ...rest }) => {
          return { ...rest, time: time + 15}
        }),
        frames: frames[i].map(([
          intensity,
          nCandidates,
          strength,
          frequency,
        ]) => {
          return intensity > 0 ? frequency : 0
        }),
        label: labels[i],
        ...rest
      }))

    })

    // Join all the speaker samples into one array
    .reduce((data, samples, i) => {
      return data.concat(samples);
    }, [])

    .filter(({ speaker, label }) => {
      // console.log({ speaker, label })
      return ![
        '0/107', '0/253', '0/393', '0/455', '0/480', '0/632', '0/658',
        '0/778', '0/801', '0/835', '0/840', '3/151', '3/341', '4/538',
        '5/9'  , '5/10' , '5/500', '5/510', '5/516', '5/517', '5/520', '5/523',
      ].includes(`${speaker}/${label}`)
    })
    .slice(...process.argv.slice(2))

  // Flattens samples into syllables, and generates curve coefficients for each syllable
, generate)